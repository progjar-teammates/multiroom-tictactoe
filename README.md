# Tic Tac Toe Online Multiplayer Game

A multithreaded tic-tac-toe game that support multiple lobbies for playing the game.

| Nama                               | NRP        |
|------------------------------------|------------|
| Mohammad Fadhil Rasyidin Parinduri | 5025201131 |
| Hidayatullah                       | 5025201031 |
| Nuzul Abatony                      | 5025201107 |
| Dhani Rizki Adriananta C. T. P.    | 5025201226 |